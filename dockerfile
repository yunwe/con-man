# 使用官方Python镜像作为基础镜像
FROM python:3.8-slim-buster
# 配置时区和安装所需的包
RUN ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo Asia/Shanghai > /etc/timezone
# 设置工作目录
WORKDIR /app
# 将当前目录内容复制到工作目录中
COPY . /app
RUN pip3 install --no-cache-dir -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt && \
    rm -rf /root/.cache/pip && \
    adduser conman
# 将工作目录的所有权更改为非root用户
RUN chown -R conman:conman /app/
# 切换到非root用户
USER conman
# 暴露端口5000供应用使用
EXPOSE 5000
# 在容器启动时运行Python应用
CMD ["python3", "./app.py"]
