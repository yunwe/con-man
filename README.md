## 项目介绍

### 项目名字由来

**ConMan** (Container Manager的缩写)，取名困难症，这个名字还是GPT生成的

### 为什么会有这个项目

某些项目无法使用Kubernetes，又必须使用容器，为了方便快速从公司的Jenkins推送代码到项目上

### 怎么用

在Jenkins构建好镜像后，使用curl给ConMan应用发送Post请求，请求中需要传递一个json，主要字段是容器名、镜像名、端口等。

## 部署

### 镜像准备

构建镜像

```
docker build -t  reg.harbor.com/docker-hub/conman:v0.1 .
```

或者使用我的镜像：

```
registry.cn-hangzhou.aliyuncs.com/image-acr/docker-hub:conman-v0.1
```

### 运行

```shell
docker run -d --name conman --restart=always \
-p 5001:5000 \
registry.cn-hangzhou.aliyuncs.com/image-acr/docker-hub:conman-v0.1
```

## 示例

```shell
curl -X POST http://192.168.1.1/update_container -H "Content-Type: application/json" -d '{
    "token": "QP77ZUFXNO0PO9AWVPSYLS5WCXUSCA1NCL2BSLV8"
    "dockerUrl": "192.168.3.24:2375",
    "volumes": {
        "/mnt/logs":"/data/logs",
        "/mnt/conf":"/data/conf"
    },
    "containerName":"testNginx",
    "networkMode": "bridge",
    "image": "nginx:latest",
    "env": {
        "TZ": "Asia/Shanghai",
        "MYSQL_ROOT_PASSWORD": "Tuf#8kgc"
    },
    "ports": {
        "80/tcp": 80,
        "9090/tcp":9090
    },
    "restart":"always"
}'

```
参数说明：

```json
{
    "token": "QP77ZUFXNO0PO9AWVPSYLS5WCXUSCA1NCL2BSLV8",
    "dockerUrl": "192.168.3.24:2375",  # Docker地址
    "volumes": {
        "/mnt/logs":"/data/logs", # 文件挂载，可选
        "/mnt/conf":"/data/conf"
    },
    "containerName":"testNginx",  # 容器名称，必选
    "networkMode": "bridge",      # 网络策略，可选
    "image": "nginx:latest",      # 镜像地址，必选
    "env": {                      # 环境变量，可选
        "TZ": "Asia/Shanghai",
        "test": "123456zzz"
    },
    "ports": {
        "80/tcp":80,
        "9090/tcp":9090               # 端口映射，可选
    }, 
    "restart":"always"            # 重启策略，可选
}
```

可在Docker的启动文件修改参数，让外部访问docker

`vim /usr/lib/systemd/system/docker.service`

```ini
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target docker.socket firewalld.service containerd.service time-set.target
Wants=network-online.target containerd.service
Requires=docker.socket

[Service]
Type=notify
# the default is not to use systemd for cgroups because the delegate issues still
# exists and systemd currently does not support the cgroup feature set required
# for containers run by docker
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H fd:// --containerd=/run/containerd/containerd.sock
ExecReload=/bin/kill -s HUP $MAINPID
TimeoutStartSec=0
RestartSec=2
Restart=always

# Note that StartLimit* options were moved from "Service" to "Unit" in systemd 229.
# Both the old, and new location are accepted by systemd 229 and up, so using the old location
# to make them work for either version of systemd.
StartLimitBurst=3

# Note that StartLimitInterval was renamed to StartLimitIntervalSec in systemd 230.
# Both the old, and new name are accepted by systemd 230 and up, so using the old name to make
# this option work for either version of systemd.
StartLimitInterval=60s

# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity

# Comment TasksMax if your systemd version does not support it.
# Only systemd 226 and above support this option.
TasksMax=infinity

# set delegate yes so that systemd does not reset the cgroups of docker containers
Delegate=yes

# kill only the docker process, not all processes in the cgroup
KillMode=process
OOMScoreAdjust=-500

[Install]
WantedBy=multi-user.target
```

需要重启docker

```shell
[root@anolis ~]# sudo systemctl daemon-reload
[root@anolis ~]# sudo systemctl restart docker.service
```

